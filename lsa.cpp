// Longstaff-Schwartz (2000) algorithm for valuing American options under Black-Scholes
// Fabrice Douglas Rouah, FRouah.com and Volopta.com

#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <cmath>
#include <cstdlib>
#include <cfloat>
#include <ctime>
#include <numeric>
#include <random>

using namespace std;

#include <omp.h>
#include "matops.cpp"

char PutCall;

// Generate Random matrix obtained with Black Scholes
void generate_random(const size_t NT, const size_t NP, const unsigned long seed, vector<vector<double>>&random) {
  random.resize(NT-1,vector<double>(NP));
  // Set the seed for random number generation
  mt19937 rng{seed};
  uniform_real_distribution<double> dist(0,1);
  double r1,r2;
  for (size_t t=0; t<NT-1; t++) {
    for (size_t s=0; s<NP; s++) {
      // Independent uniform random variables
      r1 = dist(rng);
      r2 = dist(rng);
      random[t][s] = sqrt(-2.0*log(r1)) * sin(2.0*M_PI*r2);
      if (t!=0) random[t][s] += random[t-1][s];
    }
  }
}

template<typename active_t>
void generate_prices(const vector<active_t>& active, const size_t cur_t, const double q, const size_t NT, const size_t NP, const vector<double>& random, vector<active_t>& St) {
  const active_t& S0 = active[0];
  const active_t& v = active[1];
  const active_t& T = active[2];
  const active_t& r = active[3];
  const active_t dt = T/NT;
  for (size_t s=0; s<NP; s++) {
    St[s] = S0*exp((r-q-0.5*v*v)*dt*cur_t + v*sqrt(dt)*random[s]);
  }
}

// Generate one Black Scholes stock price
template<typename active_t>
void generate_price(const vector<active_t>& active, const size_t cur_t, const double q, const size_t NT, const double random, active_t& St) {
  const active_t& S0 = active[0];
  const active_t& v = active[1];
  const active_t& T = active[2];
  const active_t& r = active[3];
  const active_t dt = T/NT;
  St = S0*exp((r-q-0.5*v*v)*dt*cur_t + v*sqrt(dt)*random);
}

template<typename active_t>
void setup_regression(const vector<active_t>& active, const size_t cur_t, const double q, const size_t NT, const size_t NP, const vector<double>& random, vector<active_t>& cashflow, size_t& NI, vector<active_t>& X, vector<int>& Xi, vector<active_t>& predicted_cashflow) {
  const active_t& K = active[4];
  // Path Generation
  vector<active_t> S(NP);
  generate_prices(active,cur_t,q,NT,NP,random,S);

  // Indices for stock paths in-the-money at time t
  NI = 0;
  vector<bool> I(NP);
  for (size_t s=0; s<NP; s++) {
    if (((PutCall == 'P') && (S[s] < K)) || ((PutCall == 'C') && (S[s] > K))) {
      I[s] = true;
      NI++;
    } else {
      I[s] = false;
    }
  }
  X.resize(NI);
  Xi.resize(NI);
  vector<active_t> Y(NI);
  vector<vector<active_t>> Z(NI, vector<active_t>(3));

  int i = 0;
  for (size_t s=0; s<NP; s++) {
    if (I[s]) {
      // Stock paths in-the-money at time t
      X[i] = S[s];
      Xi[i] = s;
      // Cash flows at time t+1, discounted one period
      Y[i] = cashflow[s];
      // Design matrix for regression to predict cash flows
      Z[i][0] = 1.0;
      Z[i][1] = 1.0 - X[i];
      Z[i][2] = 1.0 - 2.0*X[i] + 0.5*pow(X[i],2);
      i++;
    }
  }
  // Regression parameters and predicted cash flows
  vector<active_t> beta(NI);
  predicted_cashflow.resize(NI);
  betavec<active_t>(Z,Y,beta);
  MVecMult(Z,beta,predicted_cashflow);
}

template<typename active_t>
void longstaff_schwarz(const vector<active_t>& active, const double q, const size_t NT, const size_t NP, vector<vector<double>>& random, vector<size_t>& exercise_time, active_t& price) {
  const active_t& T = active[2];
  const active_t& r = active[3];
  const active_t& K = active[4];
  // Time increment and discount
  const active_t dt = T/NT;
  const active_t discount = exp((-r+q)*dt);
  // Allocate cash flows
  vector<active_t> cashflow(NP);
  active_t S;
  // Set the last cash flows to the intrinsic value.
  for (size_t s=0; s<NP; s++) {
    generate_price(active,NT-1,q,NT,random[NT-2][s],S);
    if (PutCall == 'P')
      if (K-S > 0.0) {
        cashflow[s] = K-S;
        exercise_time[s] = NT-1;
      } else {
        cashflow[s] = 0.0;
        exercise_time[s] = 0;
      }
    else if (PutCall == 'C') {
      if (S-K > 0.0) {
        cashflow[s] = S-K;
        exercise_time[s] = NT-1;
      } else {
        cashflow[s] = 0.0;
        exercise_time[s] = 0;
      }
    }
  }
  // Work backwards through the stock prices until time t=1.
  // We could work through to time t=0 but the regression will not be
  // of full rank at time 0, so this is cleaner.
  for (size_t t=NT-2; t>=1; t--) {
    // Discount the cash flow by one period
    for (size_t s=0; s<NP; s++)
      cashflow[s] = discount*cashflow[s];

    size_t NI = 0;
    vector<active_t> X;
    vector<int> Xi;
    vector<active_t> predicted_cashflow;
    // Setup the regression and calculate the predicted cash flow
    setup_regression(active,t,q,NT,NP,random[t-1],cashflow,NI,X,Xi,predicted_cashflow);
    // Replace cash flows with exercise value where exercise is optimal
    for (size_t s=0; s<NI; s++) {
      if ((PutCall == 'P') && (K-X[s] > predicted_cashflow[s])) {
        cashflow[Xi[s]] = K-X[s];
        exercise_time[Xi[s]] = t;
      } else if ((PutCall == 'C') && (X[s]-K > predicted_cashflow[s])) {
        cashflow[Xi[s]] = X[s]-K;
        exercise_time[Xi[s]] = t;
      }
    }
  }
  // Calculate the American price at t=1
  price = accumulate(cashflow.begin(), cashflow.end(), 0.0)/ cashflow.size();
  // Discount the price by one period
  price *= discount;
}

int main() {
  double S0, K, T, v, r, q;
  size_t NT,NP,seed;
  bool new_random;
  ifstream fin("lsa.config");
  // Stock price, strike, maturity, volatility, risk free rate, dividend yield
  fin.ignore(20,':'); fin >> S0;
  fin.ignore(20,':'); fin >> K;
  fin.ignore(20,':'); fin >> T;
  fin.ignore(20,':'); fin >> v;
  fin.ignore(20,':'); fin >> r;
  fin.ignore(20,':'); fin >> q;
  fin.ignore(20,':'); fin >> PutCall;
  fin.ignore(20,':'); fin >> NT;
  fin.ignore(20,':'); fin >> NP;
  fin.ignore(20,':'); fin >> new_random;
  fin.close();
// Generate a new seed
  if (new_random) {
    ofstream ofs("seed.data");
    random_device dev;
    seed = dev();
    ofs << seed << endl;
    ofs.close();
  } else {
    ifstream ifs("seed.data");
    ifs >> seed;
    ifs.close();
  }

  // Allocation
  double price;
  const vector<double> active{S0, v, T, r, K};

  cout << "Initialization done!\n";
// Function Evaluation
  vector<size_t> exercise_time(NP);
  // Generate the random numbers
  vector<vector<double>> random;
  generate_random(NT,NP,seed,random);
  // Obtain the American LSM price
  longstaff_schwarz(active,q,NT,NP,random,exercise_time,price);

  // Output the results
  cout << "Longstaff-Schwartz American option approximation\n";
  cout << "--------------------------------------------------------\n";
  cout << setprecision(2) << fixed;
  cout << "Setup for ";
  if (PutCall == 'P') cout << "put-option\n";
  else cout << "call-option\n";
  cout << "--------------------------------------------------------\n";
  cout << "Stock price\t\t" << S0 << "\t\tStrike price\t" << K;
  cout << "\nRisk free rate\t\t" << r << "\t\tDividend yield\t" << q;
  cout << "\nMaturity\t\t" << T << "\t\tVolatility\t" << v;
  cout << "\nNumber timesteps\t" << NT << "\t\tNumber paths\t" << NP;
  cout << "\nSeed\t" << seed;
  cout << setprecision(6) << fixed;
  cout << "\n--------------------------------------------------------\n";
  cout << "Price: " << price;
  cout << "\n--------------------------------------------------------\n";
}
