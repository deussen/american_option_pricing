// LU decomposition;
template<typename TYPE>
void LU(vector<vector<TYPE> >& A,vector<vector<TYPE> >& L,vector<vector<TYPE> >& U) {
  int N = A.size();
  vector<vector<TYPE> >  B(N,vector<TYPE> (N));
  for(int i=0;i<N;i++)
    for(int j=0;j<N;j++)
      B[i][j] = A[i][j];

  for(int k=0;k<N-1;k++) {
    const TYPE Bkk = 1.0/B[k][k];
    for(int i=k+1;i<N;i++)
      B[i][k] = B[i][k] * Bkk;
    for(int j=k+1;j<N;j++){
          const TYPE Bkj = B[k][j];
      for(int i=k+1;i<N;i++)
        B[i][j] = B[i][j] - B[i][k]*Bkj;
    }
  }
  for(int i=0;i<=N-1;i++)	{
    L[i][i] = 1.0;
    for(int j=0;j<=N-1;j++) {
      if(i>j)
        L[i][j] = B[i][j];
      else
        U[i][j] = B[i][j];
    }
  }
}

// Inverse of an upper triangular matrix
template <typename TYPE>
void MatUpTriangleInv(vector<vector<TYPE> >& U,vector<vector<TYPE> >& V) {
  int N = U.size();
  for(int j=N-1;j>=0;j--) {
    V[j][j] = 1.0 / U[j][j];
    for(int i=j-1;i>=0;i--){
      TYPE Uii = 1.0 / U[i][i];
      for(int k=i+1;k<=j;k++){
        V[i][j] -= Uii * U[i][k] * V[k][j];
      }
    }
  }
}

// Inverse of a lower triangular matrix
template <typename TYPE>
void  MatLowTriangleInv(vector<vector<TYPE> >& L,vector<vector<TYPE> >& V) {
  int N = L.size();
  for(int i=0;i<=N-1;i++)	{
    V[i][i] = 1.0 / L[i][i];
    for(int j=i-1;j>=0;j--)
      for(int k=i-1;k>=j;k--)
        V[i][j] -= V[i][i] * L[i][k] * V[k][j];
  }
}

// Multiply two matrices together
// First matrix is (n x k), Second is (k x m)
// Resulting matrix is (n x m)
template <typename TYPE>
void MMult(vector<vector<TYPE> >& A,vector<vector<TYPE> >& B,vector<vector<TYPE> >& C) {
  int n = A.size();
  int k = B.size();
  int m = B[0].size();
  for (int j=0; j<=m-1; j++)
    for (int i=0; i<=n-1; i++) {
      C[i][j] = 0;
      for (int r=0; r<=k-1; r++)
        C[i][j] += A[i][r] * B[r][j];
    }
}

//Multiply a transpose of a matrix with the matrix itself
template <typename TYPE>
void MtM(vector<vector<TYPE> >& A, vector<vector<TYPE> >& C) {
  int n = A.size();
  int m = A[0].size();

  for(int j=0; j<m; j++){
    for(int i=0; i<m; i++){
      C[i][j] = 0;
    }
  }

  //C[0][0] = n, due to A[:][0] = 1
  C[0][0] = n;

  for(int i=1; i<m;i++){
    for(int r=0; r<n; r++){
      C[i][0] += A[r][i];
    }
    C[0][i] = C[i][0];
  }

  for(int j=1; j<m; j++){
    for(int i=1; i<=j; i++){
      for(int r=0; r<n; r++){
        C[i][j] += A[r][i] * A[r][j];
      }
      C[j][i] = C[i][j];
    }
  }
}

// Inverse of a matrix through LU decomposition
template <typename TYPE>
void MInvLU(vector<vector<TYPE> >& A, vector<vector<TYPE> >& Ainv) {
  int N = A.size();
  vector<vector<TYPE> > L(N,vector<TYPE> (N));
  vector<vector<TYPE> > U(N,vector<TYPE> (N));
  vector<vector<TYPE> > Linv(N,vector<TYPE> (N));
  vector<vector<TYPE> > Uinv(N,vector<TYPE> (N));
  LU(A,L,U);
  MatLowTriangleInv(L,Linv);
  MatUpTriangleInv(U,Uinv);
  MMult(Uinv,Linv,Ainv);
}

// Transpose of a matrix
template <typename TYPE>
void MTrans(vector<vector<TYPE> >& A, vector<vector<TYPE> >& C) {
  int rows = A.size();
  int cols = A[0].size();
  for (int i=0; i<cols; i++)
    for (int j=0; j<rows; j++)
      C[i][j] = A[j][i];
}

// Multiply a matrix by a vector
template <typename TYPE>
void MVecMult(vector<vector<TYPE> >& X, vector<TYPE>& Y, vector<TYPE>& XY) {
  int rows = X.size();
  int cols = X[0].size();
  for (int r=0; r<rows; r++) {
    XY[r] = 0.0;
    for (int c=0; c<cols; c++)
      XY[r] += X[r][c] * Y[c];
  }
}

// Multiply a transposed matrix by a vector
template <typename TYPE>
void MtVec(vector<vector<TYPE> >& X, vector<TYPE>& Y, vector<TYPE>& XY) {
  int cols = X.size();
  int rows = X[0].size();
  for (int r=0; r<rows; r++) {
    XY[r] = 0.0;
    for (int c=0; c<cols; c++)
      XY[r] += X[c][r] * Y[c];
  }
}

// Regression beta coefficients
template <typename TYPE>
void betavec(vector<vector<TYPE> >& X, vector<TYPE>& Y, vector<TYPE>& V) {
  int cols = X[0].size();
  vector<vector<TYPE> > XtX(cols, vector<TYPE> (cols));
  vector<vector<TYPE> > XtXinv(cols, vector<TYPE> (cols));
  vector<TYPE> XtY(cols);

  MtM(X,XtX); MtVec(X,Y,XtY);

  MInvLU(XtX,XtXinv);
  MVecMult(XtXinv,XtY, V);
}
